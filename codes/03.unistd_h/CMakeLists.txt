﻿# CMakeList.txt : Top-level CMake project file, do global configuration
# and include sub-projects here.
#
cmake_minimum_required (VERSION 3.8)

project ("unistd" VERSION 1.0.0 DESCRIPTION "unistd implementation")

set(PACKAGE_NAME "unistd")
set(PACKAGE_VERSION "${PROJECT_VERSION}")
set(PACKAGE_STRING "${PACKAGE_NAME} ${PACKAGE_VERSION}")
set(PACKAGE_TARNAME "${PACKAGE_NAME}")
set(CMAKE_INSTALL_LIBDIR C:/winux/usr/lib)

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

add_library(unistd SHARED
    "config.h" "unistd.h" getpagesize.c)

set(EXTRA_COMPILE_FLAGS)
if(FLAG_NO_STRICT_ALIASING)
    set(EXTRA_COMPILE_FLAGS "${EXTRA_COMPILE_FLAGS} -fno-strict-aliasing")
endif()
if(FLAG_VISIBILITY)
  add_definitions(-DXML_ENABLE_VISIBILITY=1)
  set(EXTRA_COMPILE_FLAGS "${EXTRA_COMPILE_FLAGS} -fvisibility=hidden")
endif()
if(MINGW)
    # Without __USE_MINGW_ANSI_STDIO the compiler produces a false positive
    set(EXTRA_COMPILE_FLAGS "${EXTRA_COMPILE_FLAGS} -Wno-pedantic-ms-format")
endif()
if (EXPAT_WARNINGS_AS_ERRORS)
    if(MSVC)
        add_definitions(/WX)
    else()
        set(EXTRA_COMPILE_FLAGS "${EXTRA_COMPILE_FLAGS} -Werror")
    endif()
endif()
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${EXTRA_COMPILE_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${EXTRA_COMPILE_FLAGS}")

include_directories(${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/lib C:/Winux/usr/lib C:/Winux/usr/include)

set_target_properties(unistd PROPERTIES VERSION ${PROJECT_VERSION})
set_target_properties(unistd PROPERTIES SOVERSION 1)
set_target_properties(unistd PROPERTIES PUBLIC_HEADER "unistd.h")

install(TARGETS unistd
    RUNTIME DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

configure_file(unistd.pc.in unistd.pc @ONLY)
install(FILES ${CMAKE_BINARY_DIR}/unistd.pc DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/pkgconfig)
