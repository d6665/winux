# Winux

Winux is a native Linux APIs that run in Windows system. The maion purpose is to fully support Linux applications on windows.

## Why Winux

Let compare current solution
| Feature | WSL 1 | WSL 2 | Winux |
|---------|-------|-------|-------|
| Integration between Windows and Linux | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Fast boot times | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Small resource foot print compared to traditional Virtual Machines | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Same user with windows | :x: | :x: | :white_check_mark: |
| Runs with current versions of VMware and VirtualBox | :x: | :white_check_mark: | :x: |
| Managed VM | :x: | :white_check_mark: | :x: |
| Full Linux APIs | :x: | :x: | :white_check_mark: |
| Multiple difference instance of Linux distributions | :x: | :x: | :white_check_mark: |
| Full Linux UI Environments | :x: | :x: | :white_check_mark: |
| Full system call compatibility | :x: | :white_check_mark: | :white_check_mark: |
| Performance across OS file systems | :white_check_mark: | :x: | :white_check_mark: |
| Android support | :x: | :white_check_mark: | :x: |

With these functionalities, Winux will help user can:

- Run Linux app in windows with maximize performance.
- Integrated Linux UI with Windows UI.
- Open sources apps, which currently running on Linux only, will have more platform in the market.
- Can run with multiple difference desktop environment at the same time.

## How to do it

Recompile Linux kernel as native platform in Windows.

## Contact info

Name: Trung

Email: doanbaotrung@gmail.com
